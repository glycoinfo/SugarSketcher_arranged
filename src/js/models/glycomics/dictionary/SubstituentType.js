/**
 * Author:  Davide Alocci
 * Version: 0.0.1
 */

import {Enum} from 'enumify';

export default class SubstituentType extends Enum {}

SubstituentType.initEnum({
    Acetyl: {
        label: 'Ac',
        linkagetype: 'o'
    },
    Bromo: {
        label: 'Br',
        linkagetype: 'd'
    },
    Chloro: {
        label: 'Cl',
        linkagetype: 'd'
    },
    Ethyl: {
        label: 'Et',
        linkagetype: 'o'
    },
    Ethanolamine: {
        label : 'ETA',
        linkagetype: 'o'
    },
    Fluoro: {
        label: 'F',
        linkagetype: 'd'
    },
    Formyl: {
        label: 'Formyl',
        linkagetype: 'o'
    },
    Hydroxymethyl: {
        label: 'HMe',
        linkagetype: 'o'
    },
    Imino: {
        label: 'Imino',
        linkagetype: 'd'
    },
    RLactate1: {
        label: 'RLac1',
        linkagetype: 'o'
    },
    SLactate1: {
        label: 'SLac1',
        linkagetype: 'o'
    },
    Amino: {
        label: 'N',
        linkagetype: 'd'
    },
    Methyl: {
        label: 'Me',
        linkagetype: 'd'
    },
    NAcetyl: {
        label: 'NAc',
        linkagetype: 'd'
    },
    NAlanine: {
        label: 'NAla',
        linkagetype: 'd'
    },
    NFormyl: {
        label: 'NFormyl',
        linkagetype: 'd'
    },
    NGlycolyl: {
        label: 'NGc',
        linkagetype: 'd'
    },
    NMethyl: {
        label: 'NMe',
        linkagetype: 'd'
    },
    NSuccinate: {
        label: 'NSuc',
        linkagetype: 'd'
    },
    NSulfate: {
        label: 'NS',
        linkagetype: 'd'
    },
    NTrifluoroacetyl: {
        label: 'NTFA',
        linkagetype: 'd'
    },
    Nitrate: {
        label: 'NO2',
        linkagetype: 'd'
    },
    Phosphate: {
        label: 'P',
        linkagetype: 'o'
    },
    Pyruvate: {
        label: 'Pyr',
        linkagetype: 'o'
    },
    Sulfate: {
        label: 'S',
        linkagetype: 'o'
    },
    Thio: {
        label: 'Thio',
        linkagetype: 'd'
    },
    RPyruvate: {
        label: 'RPyr',
        linkagetype: 'o'
    },
    SPyruvate: {
        label: 'SPyr',
        linkagetype: 'o'
    },
    RLactate2: {
        label: 'RLac2',
        linkagetype: 'o'
    },
    SLactate2: {
        label: 'SLac2',
        linkagetype: 'o'
    },
    Glycolyl: {
        label: 'Gc',
        linkagetype: 'o'
    },
    Iodo: {
        label: 'I',
        linkagetype: 'd'
    },
    NDimethyl: {
        label: 'NDiMe',
        linkagetype: 'd'
    },
    OMethyl: {
        label: 'OMe',
        linkagetype: 'o'
    },
    ONitrate: {
        label: 'NO3',
        linkagetype: 'o'
    }
});
